# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2021-2024 Lars Grüter
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""File based interface to share messages from root to users."""

import json
import logging
import os
import shutil
import time
from pathlib import Path
from typing import Any, Generator


logger = logging.getLogger(__name__)


def xdg_state_home() -> Path:
    """Fetch path to directory for user-specific state data.

    This directory is defined in the XDG Base Directory Specification [1] and
    usually stored inside the environment variable XDG_STATE_HOME.

    May raise a RuntimeError if XDG_STATE_HOME is unset and the users home
    directory cannot be resolved.

    [1] https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
    """
    state_home = os.environ.get("XDG_STATE_HOME", "")
    if state_home == "":
        path = Path.home() / ".local/state"
    else:
        path = Path(state_home)
    return path


class MessageBoard:
    """File based message board to be read by multiple clients.

    This class implements an interface to pass messages between separate
    processes via the file system. One sender posts messages on a board which
    persist via the file system and are stored at `board_dir`.

    See `BoardReader` on how to read this board and usage examples.
    """

    default_board_dir = Path("/var/spool/pacautomation/notify")

    def __init__(self, *, board_dir: Path):
        self.board_dir = board_dir

    def post(self, message: dict[Any, Any]) -> None:
        """Post a new message onto the board."""
        timestamp = time.time_ns()
        try:
            board_file = self.board_dir / str(timestamp)
            with board_file.open("x") as io:
                io.write(json.dumps(message))
            logger.debug("created message in %s", board_file)
        except PermissionError:
            logging.warning(
                "insufficient permissions to create file in %s",
                self.board_dir,
            )

    def clear(self) -> None:
        """Clear the board."""
        try:
            for file in self.board_dir.iterdir():
                file.unlink()
        except PermissionError:
            logging.warning(
                "insufficient permissions to clear directory %s",
                self.board_dir,
            )


class MessageBoardReader:
    """Client that reads posts on a `MessageBoard` only once.

    This class reads posts on a board at `board_dir` that is managed by the
    `MessageBoard` class. Each post that was read is remembered and archived, so
    that they are shown only once for each client. For this purpose each client
    uses its own `archive_dir`.

    Use `~.user_archive_dir` to determine a unique archive dir in each Linux
    user's $XDG_STATE_HOME.

    Examples:
        >>> board_dir = Path("/tmp/notify")
        >>> board_dir.mkdir(exist_ok=True)
        >>> board = MessageBoard(board_dir=board_dir)
        >>> board.clear()
        >>> board.post({"message": "Hello!"})
        >>> reader_foo = MessageBoardReader(
        ...     board_dir=board_dir, archive_dir=Path("/tmp/foo/notified")
        ... )
        >>> list(reader_foo)
        [{'message': 'Hello!'}]
        >>> board.post({"message": "Hello again!"})
        >>> reader_bar = MessageBoardReader(
        ...     board_dir=board_dir, archive_dir=Path("/tmp/bar/notified")
        ... )
        >>> list(reader_bar)
        [{'message': 'Hello!'}, {'message': 'Hello again!'}]
        >>> list(reader_foo)
        [{'message': 'Hello again!'}]
    """

    @staticmethod
    def default_per_user_archive_dir() -> Path:
        """Return the archive dir in XDG_STATE_HOME."""
        return xdg_state_home() / "pacautomation/notified"

    def __init__(self, *, board_dir: Path, archive_dir: Path):
        self.board_dir = board_dir
        self.archive_dir = archive_dir
        self.archive_dir.mkdir(parents=True, exist_ok=True)
        self._clean_archive()

    def __iter__(self) -> Generator[dict[Any, Any], None, None]:
        for board_file in sorted(self.board_dir.iterdir()):
            timestamp = board_file.name
            archive_file = self.archive_dir / timestamp
            if archive_file.exists():
                logger.debug("skipping known message in %s", archive_file)
                continue
            with board_file.open("r") as io:
                message = json.load(io)
            shutil.copyfile(board_file, archive_file)
            logger.debug("found new message in %s", board_file)
            yield message

    def _clean_archive(self) -> None:
        """Remove archived entries that no longer exist on the board."""
        for archived_file in self.archive_dir.iterdir():
            board_file = self.board_dir / archived_file.name
            if not board_file.exists():
                archived_file.unlink()
                logger.debug("removed file %s", archived_file)
