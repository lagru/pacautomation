# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2021-2024 Lars Grüter
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Check for pending desktop notifications that were queued by pacautomation.

Checks for pending notifications that haven't yet been shown to the invoking
user. See https://gitlab.com/lagru/pacautomation for more.
"""


import argparse
import logging
import logging.config
import subprocess
import sys
import traceback
from contextlib import contextmanager
from typing import Any, Generator, Final

from pacautomation.message_board import MessageBoard, MessageBoardReader


logger: Final = logging.getLogger(__name__)


def gdbus_notify_template(
    *, summary: str, body: str, event: str, icon: str
) -> list[str]:
    """Create command to send notifications with gdbus from template.

    Creates a list of args to pass to `subprocess.run`. See `try_notify`
    for a description of the arguments.

    Needs to be run as the receiving user with correct DISPLAY and
    DBUS_SESSION_BUS_ADDRESS set!

    See https://wiki.archlinux.org/title/Desktop_notifications#Bash
    """
    cmd = [
        "gdbus",
        "call",
        "--session",
        "--dest=org.freedesktop.Notifications",
        "--object-path=/org/freedesktop/Notifications",
        "--method=org.freedesktop.Notifications.Notify",
        "pacautomation",
        "0",
        icon,
        summary,
        body,
        "[]",
        f'{{"x-kde-appname": <"pacautomation">, "x-kde-eventId": <"{event}">, '
        f'"urgency": <1>}}',
        "int32 -1",
    ]
    return cmd


def try_notify(*, summary: str, body: str, event: str, icon: str) -> None:
    """Try to send a desktop notification to the current user.

    The content of the notification is defined by a short `summary` and a more
    detailed `body`; both may include ' or " characters. `event` defines an
    optional type (see matching KDE-specific .notifyrc file) and `icon` may
    contain the name of an available installed icon.
    """
    logger.info("new notification: %s %s", summary, body.replace("\n", " "))
    cmd = gdbus_notify_template(
        summary=summary,
        body=body,
        event=event,
        icon=icon,
    )
    subprocess.run(
        cmd,
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        text=True,
    )


def parse_command_line() -> dict[str, Any]:
    """Define and parse command line options."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "--debug", action="store_true", help="show debugging information"
    )
    kwargs = vars(parser.parse_args())
    return kwargs


def setup_logging(*, debug: bool) -> None:
    """Setup and configure logging for STDOUT.

    `debug` determines the log level for the standard output.
    """
    logging.config.dictConfig(
        {
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {
                "simple": {"format": "%(levelname)s: %(message)s"},
            },
            "handlers": {
                "console": {
                    "class": "logging.StreamHandler",
                    "level": logging.DEBUG if debug else logging.INFO,
                    "formatter": "simple",
                    "stream": "ext://sys.stdout",
                },
            },
            "root": {"level": logging.DEBUG, "handlers": ["console"]},
        }
    )


@contextmanager
def handle_exceptions() -> Generator[None, None, None]:
    """Handle (un)expected exceptions in `run_app`."""
    try:
        yield
    except (SystemExit, KeyboardInterrupt):
        raise
    except Exception as e:
        if isinstance(e, subprocess.CalledProcessError):
            summary = "Error in subprocess while running pacautomation-notifier!"
            msg = f"'{e.cmd}' returned '{e.returncode}', output:\n{e.stdout}"
        else:
            summary = "Error while running pacautomation-notifier!"
            msg = f"unexpected error: {e}\n{traceback.format_exc()}"
        logger.error(msg)
        try_notify(
            summary=summary,
            body=msg,
            event="Error",
            icon="dialog-error.svg",
        )
        sys.exit(1)


def main(**kw: dict[str, Any]) -> None:
    """Main function of the notifier.

    Keyword arguments are supplied by `parse_command_line()`.
    """
    logger.debug("started script with kwargs: %s", kw)
    reader = MessageBoardReader(
        board_dir=MessageBoard.default_board_dir,
        archive_dir=MessageBoardReader.default_per_user_archive_dir(),
    )
    for notification in reader:
        try_notify(**notification)
    logger.debug("finished successfully")


@handle_exceptions()
def run() -> None:
    """Run the notifier and read input from stdin."""
    kwargs = parse_command_line()
    setup_logging(debug=kwargs["debug"])
    main(**kwargs)
