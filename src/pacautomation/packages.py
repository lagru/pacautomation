# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2021-2024 Lars Grüter
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Functionality related to Arch Linux's packages."""


import logging
import re
import subprocess
from dataclasses import dataclass, field
from datetime import datetime
from functools import lru_cache
from pathlib import Path
from typing import Optional, Final
from urllib.parse import urlparse


# Regex to parse pacman's log, used in `removed_packages`.
MATCH_UPGRADE: Final = (
    r"^\[(?P<stamp>[^]]*)\] \[ALPM\] upgraded "
    r"(?P<name>[^\s]*) \((?P<version>[^\s]*) -> [^)]*\)$$"
)
MATCH_REMOVE: Final = (
    r"^\[(?P<stamp>[^]]*)\] \[ALPM\] removed "
    r"(?P<name>[^\s]*) \((?P<version>[^\s]*)\)$"
)


logger: Final = logging.getLogger(__name__)


@dataclass(frozen=True, kw_only=True, slots=True)
class Package:
    """A package uniquely defined by a `name` and `version`.

    Optionally stores the path to the packages archive `file` and the time
    the package was `removed_on`. However, the content of these is ignored
    for comparisons and hashing which makes this container very useful for set
    operations. Be mindful to emulate operators such as "&" with the "-"
    operator if you want to guarantee that objects (and their unhashed
    attributes) in a specific set take precedence.

    Packages can be compared. However, if the package name is identical a slow
    call to the CLI program "vercmp" is made.

    Note that for the purpose of this class, the build number is considered
    part of the `version` and stored in the attribute of the same name.
    """

    name: str
    version: str
    file: Optional[Path] = field(default=None, compare=False)
    removed_on: Optional[datetime] = field(default=None, compare=False)

    @staticmethod
    def _parse_archive_name(name: str) -> tuple[str, str]:
        parts = name.split("-")
        if len(parts) < 4:
            raise RuntimeError(
                f"expected package file name to split into >= 4 parts, got {parts!r}"
            )
        package_name = "-".join(parts[:-3])
        package_version = "-".join(parts[-3:-1])
        return package_name, package_version

    @classmethod
    def from_file(cls, path: Path) -> "Package":
        """Create `Package` from an archive file `path`.

        Assumes a file naming format as returned by `makepkg`.
        """
        package_name, package_version = cls._parse_archive_name(path.name)
        return cls(name=package_name, version=package_version, file=path)

    @classmethod
    def from_url(cls, url: str) -> "Package":
        scheme, _, path_str, *_ = urlparse(url)
        path = Path(path_str)
        package_name, package_version = cls._parse_archive_name(path.name)
        file = path if path.is_file() else None
        return cls(name=package_name, version=package_version, file=file)

    @classmethod
    def from_pacman_query(cls, line: str) -> "Package":
        """Create `Package` from a line output by "pacman -Q"."""
        package_name, package_version = line.split(" ")
        return cls(name=package_name, version=package_version)

    @property
    def signature_file(self) -> Optional[Path]:
        """Path to the packages signature file, if it exists."""
        if self.file is not None:
            signature_path = self.file.with_suffix(self.file.suffix + ".sig")
            if signature_path.exists():
                return signature_path
        return None

    @property
    def removed_on_assert_datetime(self) -> datetime:
        """Return `removed_on` and ensure that it is of type `datetime`.

        Simple wrapper that allows type inference within a list comprehension or
        similar.
        """
        assert isinstance(self.removed_on, datetime)
        return self.removed_on

    def __str__(self) -> str:
        return f"{self.name} {self.version}"

    def __lt__(self, other: "Package") -> bool:
        if self.name == other.name:
            result = subprocess.run(
                ("vercmp", self.version, other.version),
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
            )
            return int(result.stdout) == -1
        else:
            return self.name < other.name


def run_pacman(*args: str, check: bool = True) -> str:
    """Run pacman with given `args` and return its output.

    Verifies that the process exits with zero if `check` is true.
    """
    result = subprocess.run(
        ("pacman",) + args,
        check=check,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        text=True,
        timeout=600,
    )
    logger.debug("'%s' succeeded, output: \n%s", result.args, result.stdout)
    return result.stdout if result.stdout else ""


def _pacman_config(option_name: str) -> str:
    """Identify paths to pacman's cache, log and database from its output."""
    result = subprocess.run(
        ("pacman-conf", option_name),
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        text=True,
        check=True,
    )
    option_value = result.stdout.strip()
    logger.debug("parsed pacman-config: %s = %s", option_name, option_value)
    return option_value


@lru_cache
def pacman_db_dir() -> Path:
    """Return path to the system's package database."""
    path = Path(_pacman_config("DBPath"))
    if not path.is_dir() or not path.exists():
        raise ValueError(f"option 'DBPath = {path}' isn't a directory or doesn't exist")
    return path


@lru_cache
def pacman_cache_dirs() -> list[Path]:
    """Return list of paths to system's package caches."""
    paths_str = _pacman_config("CacheDir")
    paths = [Path(p) for p in paths_str.split("\n")]
    for path in paths:
        if not path.is_dir() or not path.exists():
            raise ValueError(
                f"option 'CacheDir = {path}' isn't a director or doesn't exist"
            )
    return paths


@lru_cache
def pacman_log_file() -> Path:
    """Return path to pacman's log file."""
    path = Path(_pacman_config("LogFile"))
    if not path.is_file() or not path.exists():
        raise ValueError(f"option 'LogFile = {path}' isn't a file or doesn't exist")
    return path


def pending_packages(db: Path, log_file: Path) -> set[Package]:
    """Identify the set of upgradeable packages using `db`.

    Redirect pacman's logging to `log_file`.
    """
    # Get package files pending for installation / upgrade
    output = run_pacman("--dbpath", str(db), "--logfile", str(log_file), "-Supq")
    if output:
        urls = output.strip().split("\n")
        pending = [Package.from_url(url) for url in urls]
    else:
        pending = []
    return set(pending)


def installed_packages() -> set[Package]:
    """Identify set of currently installed packages."""
    # Get currently installed packages
    output = run_pacman("-Q")
    if not output:
        raise RuntimeError("didn't identify any installed packages")
    lines = output.strip().split("\n")
    installed = set(Package.from_pacman_query(line) for line in lines)
    return installed


def orphaned_packages() -> set[Package]:
    """Identify and return a set of orphaned packages.

    Orphans are packages which are not required as dependencies and weren't
    explicitly installed.
    """
    output = run_pacman("-Qtd", check=False)
    if output:
        lines = output.strip().split("\n")
        orphans = set(Package.from_pacman_query(line) for line in lines)
    else:
        orphans = set()
    return orphans


def removed_packages() -> set[Package]:
    """Identify and return set of removed packages using pacman's log file.

    The most recent removal time takes precedence.
    """
    with pacman_log_file().open("r") as io:
        log = io.read()

    lines = re.findall(MATCH_REMOVE, log, re.MULTILINE)
    lines += re.findall(MATCH_UPGRADE, log, re.MULTILINE)

    date_format = "%Y-%m-%dT%H:%M:%S%z"
    removed = (
        Package(
            name=match[1],
            version=match[2],
            removed_on=datetime.strptime(match[0], date_format),
        )
        for match in lines
    )

    # Sets don't replace an item if it already exists, sort by removal time
    # so that the most recent removal takes precedence
    last_removed = set(
        sorted(removed, key=lambda x: x.removed_on_assert_datetime, reverse=True)
    )
    return last_removed


def cached_packages() -> set[Package]:
    """Identify and return set of cached packages."""
    cached: set[Package] = set()
    for path in pacman_cache_dirs():
        files = path.glob("*.pkg.tar.zst")
        cached.update(Package.from_file(file) for file in files)
    return cached
