# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2021-2024 Lars Grüter
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Run pacautomation for automated maintenance around pacman.

This script implements a background service that can automate periodic tasks
around pacman for your convenience. The following tasks are executed when this
script is called:

- Download upgrades for all installed packages with a temporary package
  database. The temporary database avoids the danger of partial upgrades.
- Clean pacman's cache by removing packages that aren't pending upgrades, are
  not currently installed and who are older than a configurable age.
- Check for unused/orphaned packages.
- Check for pacnew, pacsave and pacorig files that need to be merged manually.

As this script is intended to be used as an automated background process, its
CLI is kept to a simple minimum. Instead, it can be configured with a config
file which is by default located at /etc/pacautomation.conf or overridden with
the --config option.

See https://gitlab.com/lagru/pacautomation for more.
"""


import argparse
import logging
import logging.config
import logging.handlers
import shutil
import socket
import subprocess
import sys
import traceback
from contextlib import contextmanager
from datetime import datetime, timedelta
from pathlib import Path
from time import sleep
from typing import Generator, Final

from pacautomation.config import Config
from pacautomation.message_board import MessageBoard
from pacautomation.packages import (
    pacman_db_dir,
    run_pacman,
    pending_packages,
    installed_packages,
    orphaned_packages,
    removed_packages,
    cached_packages,
)


logger: Final = logging.getLogger(__name__)

board: Final = MessageBoard(board_dir=MessageBoard.default_board_dir)


def format_total_size(files: list[Path]) -> str:
    """Sum and format total size of given `files`."""
    summed_size = sum(p.stat().st_size for p in files)
    if summed_size >= 2**30:
        return f"{summed_size / 2**30:.2f} GiB"
    else:
        return f"{summed_size / 2**20:.2f} MiB"


def reset_tmp_db(tmp_db: Path) -> None:
    """Reset temporary database `tmp_db` to the state of `pacman_db`."""
    if tmp_db.resolve() == pacman_db_dir().resolve():
        raise ValueError("path to temporary database points to system's database!")
    if tmp_db.is_dir():
        shutil.rmtree(tmp_db)
        logger.debug("removed old temporary database at %s", tmp_db)
    shutil.copytree(pacman_db_dir(), tmp_db)
    logger.debug("copied system's database from %s to %s", pacman_db_dir(), tmp_db)


@contextmanager
def log_new_downloads() -> Generator[None, None, None]:
    """Log any new packages in the cache.

    Compares the package cache before and after running the context. If new
    files are found log them.
    """
    old_cache = cached_packages()
    yield
    new_downloads = cached_packages() - old_cache
    files: list[Path] = []
    for package in sorted(new_downloads):
        assert isinstance(package.file, Path)
        files.append(package.file)
        if package.signature_file is not None:
            files.append(package.signature_file)
    logger.info(
        "checked for new files in cache: %i files / %s: %s",
        len(files),
        format_total_size(files),
        ", ".join(p.name for p in files),
    )


@log_new_downloads()
def try_check_upgrades(
    *,
    tmp_db: Path,
    log_file: Path,
    download: bool,
    tries: int = 3,
    retry_wait: int = 300,
) -> None:
    """Check for package upgrades.

    Copies system's current package database to a temporary directory `tmp_db`,
    then - using this temporary database - searches for and optionally downloads
    upgrades that are pending into the default cache location. pacman's output
    is redirected to `log_file` while doing so.

    In case the network connection fails, the function retries after sleeping
    for `retry_wait` seconds, until the total number of `tries` is reached. This
    is mainly useful if this script is triggered immediately after resuming /
    booting the machine and the connection is not yet up.

    Tries to send a desktop notification about potentially available upgrades.
    """
    if not tmp_db.is_dir():
        raise RuntimeError(f"no temporary copy of system's database at {tmp_db}")

    sync_option = "-Syuw" if download is True else "-Sy"

    for attempt in range(1, tries + 1):
        try:
            # Upgrade temporary database and download available packages to the
            # default cache directory implicitely
            run_pacman(
                "--dbpath",
                str(tmp_db),
                "--logfile",
                str(log_file),
                sync_option,
                "--noconfirm",
            )
        except subprocess.CalledProcessError as e:
            error_msg = (f"{e.cmd} returned '{e.returncode}', output:\n{e.stdout}",)
            if attempt < tries:
                logger.warning(
                    "checking for upgrades failed (attempt: %i/%i), retrying in %i s:\n%s",
                    attempt,
                    tries,
                    retry_wait,
                    error_msg,
                )
                sleep(retry_wait)
            else:
                logger.error(
                    "checking for upgrades: all %i attempts failed:\n%s",
                    tries,
                    error_msg,
                )
                notification = dict(
                    summary="Checking for upgrades failed",
                    body=f"Is the internet connected? See '{log_file}' for details.",
                    event="Error",
                    icon="dialog-error.svg",
                )
                board.post(notification)
                return
        else:
            break  # Stop after successful upgrade

    pending = sorted(pending_packages(db=tmp_db, log_file=log_file))
    # Report results
    logger.info(
        "checked for upgrades: %i pending: %s",
        len(pending),
        ", ".join(str(p) for p in pending),
    )
    if pending:
        notification = dict(
            summary=f"{len(pending)} package upgrade(s) available",
            body="Available upgrades:\n" + " ".join(p.name for p in pending),
            event="PendingUpgrades",
            icon="system-software-update.svg",
        )
        board.post(notification)


def clean_cache(*, expiry_age: timedelta, tmp_db: Path, log_file: Path) -> None:
    """Clean the package cache.

    Removes all packages from the cache except for
    - pending upgrades,
    - installed packages,
    - packages which were removed less than `EXPIRY_AGE` days ago.
    """
    cached = cached_packages()
    candidates = cached.copy()  # candidates for deletion

    # Keep currently installed packages
    installed = installed_packages()
    if not installed:
        raise RuntimeError("could not determine any installed package(s)")
    candidates -= installed

    # Keep pending upgrades
    pending = pending_packages(db=tmp_db, log_file=log_file)
    candidates -= pending

    # Keep removed packages for some time
    expiry_date = datetime.now().astimezone() - expiry_age
    removed = removed_packages()
    # Only check time of packages still in cache, this is effectively an
    # intersection, but we use "-" to preserve the packages in `removed` which
    # have the `removed` attribute set
    cached_removed = removed - (removed - cached)
    not_expired = {
        p for p in cached_removed if p.removed_on_assert_datetime > expiry_date
    }
    candidates -= not_expired

    # All packages in the cache should either be pending upgrades, currently installed
    # or formerly installed packages
    unexpected = cached - pending - installed - cached_removed
    if unexpected:
        logger.warning(
            "Found unexpected packages in cache (may be due to skipped upgrades): %s",
            " ".join(str(p.file) for p in sorted(unexpected)),
        )

    # Include signatures
    expired_files: list[Path] = []
    for package in sorted(candidates):
        assert package.file is not None
        expired_files.append(package.file)
        if package.signature_file is not None:
            expired_files.append(package.signature_file)

    # Count size before deletion
    size_descr = format_total_size(expired_files)
    # Finally remove expired packages
    for path in expired_files:
        path.unlink()
        logger.debug("removed %s", path)

    # Report results
    logger.info(
        "cleaned cache: removed %i files / %s: %s",
        len(expired_files),
        size_descr,
        ", ".join(p.name for p in expired_files),
    )
    if expired_files:
        notification = dict(
            summary="Cleaned package cache",
            body=f"Removed {len(expired_files)} files / {size_descr}.",
            event="CleanedCache",
            icon="dialog-positive.svg",
        )
        board.post(notification)


def check_orphans() -> None:
    """Check and notify if orphaned packages are installed."""
    orphans = sorted(orphaned_packages())
    logger.info(
        "checked for orphans: found %i: %s",
        len(orphans),
        ", ".join(str(p) for p in orphans),
    )
    if orphans:
        body = "Not required as dependencies and not explicitly installed:\n"
        body += " ".join(p.name for p in orphans)
        notification = dict(
            summary=f"Found {len(orphans)} unrequired package(s)",
            body=body,
            event="Orphans",
            icon="dialog-information.svg",
        )
        board.post(notification)


def check_pacfiles() -> list[Path]:
    """Check for pacnew, pacsave and pacorig files."""
    result = subprocess.run(
        (
            "find",
            "/etc",
            "-name",
            "*.pacnew",
            "-or",
            "-name",
            "*.pacorig",
            "-or",
            "-name",
            "*.pacsave",
            "-or",
            "-name",
            "*.pacsave.[0-9]",
        ),
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        text=True,
    )
    files = [f for f in result.stdout.strip().split("\n") if f]
    files = sorted(files)

    logger.log(
        logging.WARNING if files else logging.INFO,
        "checked for pacfiles: found %i: %s",
        len(files),
        " ".join(files),
    )
    if files:
        body = "Files found:\n" + "\n".join(files)
        notification = dict(
            summary=f"Found {len(files)} config files that need to be merged!",
            body=body,
            event="FoundPacFiles",
            icon="dialog-warning.svg",
        )
        board.post(notification)

    return [Path(f) for f in files]


def trigger_notification_sockets() -> None:
    """Send an empty Datagram to all available notification sockets.

    "notification sockets" are Unix domain sockets of type SOCK_DGRAM that are
    created for each active systemd user session by a provided unit file. They
    are found at ``/run/user/${UID}/pacautomation-notify.socket``.

    On incoming traffic, a systemd user service is started that in turn runs
    pacautomation's notification script. The actual content of the datagram is
    ignored and flushed.
    """
    addresses = Path("/run/user").glob("*/pacautomation-notify.socket")
    for address in addresses:
        logger.debug("notifying socket: %s", address)
        with socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM) as s:
            s.connect(str(address))
            s.sendall(b"")


def parse_command_line() -> tuple[bool, Path]:
    """Define and parse command line options."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "--debug", action="store_true", help="show debugging information"
    )
    parser.add_argument(
        "--config",
        help="path to alternative config file",
        default=Config.default_file_path,
        type=Path,
    )
    args = parser.parse_args()
    return args.debug, Path(args.config)


def setup_logging(*, debug: bool) -> None:
    """Setup and configure logging for STDOUT.

    `debug` determines the log level for the standard output.
    """
    logging.config.dictConfig(
        {
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {
                "simple": {"format": "%(levelname)s: %(message)s"},
            },
            "handlers": {
                "console": {
                    "class": "logging.StreamHandler",
                    "level": logging.DEBUG if debug else logging.INFO,
                    "formatter": "simple",
                    "stream": "ext://sys.stdout",
                },
            },
            "root": {"level": logging.DEBUG, "handlers": ["console"]},
        }
    )


def use_log_file(path: Path) -> None:
    """Attach a log handler to the root logger that logs to `path`."""
    handler = logging.handlers.WatchedFileHandler(filename=path)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter(
        fmt="[%(asctime)s] [%(name)s] %(levelname)s: %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S%z",
    )
    handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(handler)
    logger.debug("using log at %s", path)


@contextmanager
def handle_exceptions_and_trigger_socket() -> Generator[None, None, None]:
    """Handle (un)expected exceptions and trigger notification sockets."""
    try:
        yield
    except (SystemExit, KeyboardInterrupt):
        raise
    except Exception as error:
        post_title = "Error in pacautomation!"

        post_msg = "See systemd journal or pacautomation's log for more details."
        error_msg = f"{type(error).__name__}: {error}"
        if hasattr(error, "__notes__"):
            post_msg = " ".join(error.__notes__ + [post_msg])
        post_msg = f"<i>{error_msg}</i>\n{post_msg}"

        log_msg = f"{error_msg}\n"
        if hasattr(error, "sterr") and error.sterr:
            log_msg = f"{log_msg}sterr: '{error.sterr.strip()}'\n"
        if hasattr(error, "stdout") and error.stdout:
            log_msg = f"{log_msg}stdout: '{error.stdout.strip()}'\n"
        log_msg = f"{log_msg}{traceback.format_exc()}"

        logger.error(log_msg)
        notification = dict(
            summary=post_title,
            body=post_msg,
            event="Error",
            icon="dialog-error.svg",
        )
        board.post(notification)
        sys.exit(1)
    finally:
        trigger_notification_sockets()


def main(*, cfg: Config, tmp_db: Path = Path("/tmp/pacautomation-tmp-db")) -> None:
    """Main function of the service.

    Keyword arguments `kw` are supplied by `parse_command_line()` and
    `parse_configuration()`. `tmp_db` is used.
    """
    logger.debug("started script with: cfg=%r, tmp_db=%r", cfg, tmp_db)
    reset_tmp_db(tmp_db=tmp_db)

    if cfg.check_upgrades is True:
        try_check_upgrades(
            tmp_db=tmp_db,
            log_file=cfg.log_file,
            download=cfg.download_upgrades,
        )
    if cfg.check_orphans is True:
        check_orphans()
    if cfg.check_pacfiles is True:
        check_pacfiles()
    if cfg.clean_cache is True:
        clean_cache(
            expiry_age=timedelta(days=cfg.cache_expiry_days),
            tmp_db=tmp_db,
            log_file=cfg.log_file,
        )
    logger.debug("finished successfully")


@handle_exceptions_and_trigger_socket()
def run() -> None:
    """Run the service and read input from stdin and the config file."""
    debug, config_path = parse_command_line()
    setup_logging(debug=debug)
    board.clear()

    pacnew = Path(f"{config_path.name}.pacnew")
    if pacnew.exists():
        notification = dict(
            summary=f"Pacautomation's config file may be outdated!",
            body=f"Merge '{pacnew}' into '{config_path}'.",
            event="FoundPacFiles",
            icon="dialog-warning.svg",
        )
        board.post(notification)
    try:
        config = Config.from_file(config_path)
    except Exception as error:
        error.add_note(f"Is there a valid config file at '{config_path}'?")
        raise
    use_log_file(config.log_file)
    main(cfg=config)
