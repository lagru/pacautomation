# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2021-2024 Lars Grüter
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Configuration handling for pacautomation."""

import logging
import tomllib
from dataclasses import dataclass, fields
from pathlib import Path
from typing import Final, ClassVar, Any


logger: Final = logging.getLogger(__name__)


@dataclass(frozen=True, kw_only=True, slots=True)
class Config:
    """Configuration object for pacautomation.

    The default values for the fields and their documentation is provided by
    the config file.
    """

    default_file_path: ClassVar[Path] = Path("/etc/pacautomation.conf")
    """Default location of the system wide configuration file."""

    # Config fields
    check_upgrades: bool
    download_upgrades: bool
    check_orphans: bool
    check_pacfiles: bool
    clean_cache: bool
    cache_expiry_days: float
    log_file: Path

    @classmethod
    def from_file(cls, path: Path) -> "Config":
        """Construct a config object from a file given by `path`."""
        with path.open("rb") as io:
            parsed_content: dict[str, Any] = tomllib.load(io)
        raw_options: dict[str, Any] = parsed_content.pop("options")
        # Convert supported fields to their annotated type
        options = {
            field.name: field.type(raw_options.pop(field.name)) for field in fields(cls)
        }
        return cls(**options)

    def __post_init__(self) -> None:
        if self.log_file.is_dir():
            raise ValueError("log_file option must specify a file")
        if self.cache_expiry_days < 0:
            raise ValueError(
                f"expiry_days option must be >= 0, found: {self.cache_expiry_days}"
            )
