"""Test desktop notifications."""

import pytest

from pacautomation import notifier


class Test_try_notify_all:
    @pytest.mark.parametrize(
        "event",
        [
            "PendingUpgrades",
            "CleanedCache",
            "Error",
            "Warning",
            "Orphans",
            "FoundPacFiles",
        ],
    )
    @pytest.mark.parametrize(
        "icon",
        [
            "system-software-update.svg",
            "dialog-positive.svg",
            "dialog-information.svg",
            "dialog-warning.svg",
            "dialog-error.svg",
        ],
    )
    def test_notify(self, subprocess_run, event, icon):
        with subprocess_run() as runner:
            notifier.try_notify(
                summary=f"Summary text",
                body="Body text.",
                event=event,
                icon=icon,
            )
            assert runner.last_args == (
                [
                    "gdbus",
                    "call",
                    "--session",
                    "--dest=org.freedesktop.Notifications",
                    "--object-path=/org/freedesktop/Notifications",
                    "--method=org.freedesktop.Notifications.Notify",
                    "pacautomation",
                    "0",
                    icon,
                    "Summary text",
                    "Body text.",
                    "[]",
                    f'{{"x-kde-appname": <"pacautomation">, "x-kde-eventId": <"{event}">, '
                    '"urgency": <1>}',
                    "int32 -1",
                ],
            )


class Test_parse_command_line:
    def test_arg_debug(self, monkeypatch):
        with monkeypatch.context() as m:
            m.setattr("sys.argv", ["pacautomation-notify", "--debug"])
            kwargs = notifier.parse_command_line()
        assert kwargs == {"debug": True}

        with monkeypatch.context() as m:
            m.setattr("sys.argv", ["pacautomation-notify"])
            kwargs = notifier.parse_command_line()
        assert kwargs == {"debug": False}

    def test_arg_unknown(self, monkeypatch):
        with monkeypatch.context() as m:
            m.setattr("sys.argv", ["pacautomation-notify", "--delete-world"])
            with pytest.raises(SystemExit):
                kwargs = notifier.parse_command_line()
