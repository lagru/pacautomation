"""Fixtures and testing tools."""

import subprocess
from contextlib import contextmanager

import pytest


# TODO use https://gitlab.archlinux.org/archlinux/pytest-pacman !


class MockRunner:
    def __init__(self, *, returncode, stdout=None, stderr=None):
        self.returncode = returncode
        self.stdout = stdout
        self.stderr = stderr

        self.calls = 0
        self.last_args = None
        self.last_kwargs = None

    def run(self, *args, **kwargs):
        self.last_args = args
        self.last_kwargs = kwargs
        self.calls += 1
        return subprocess.CompletedProcess(
            args,
            returncode=self.returncode,
            stdout=self.stdout,
            stderr=self.stderr,
        )


@pytest.fixture(scope="function")
def subprocess_run(monkeypatch):
    @contextmanager
    def patcher(returncode=0, *, stdout=None, stderr=None):
        runner = MockRunner(returncode=returncode, stdout=stdout, stderr=stderr)
        with monkeypatch.context() as m:
            m.setattr("subprocess.run", runner.run)
            yield runner

    yield patcher
