"""Test `packages` module."""

from pathlib import Path

import pytest

from pacautomation.packages import (
    _pacman_config,
    pacman_db_dir,
    pacman_cache_dirs,
    pacman_log_file,
    Package,
    pending_packages,
    installed_packages,
    orphaned_packages,
)


mark_parametrize_name = pytest.mark.parametrize("name", ["foo-bar", "foo_bar", "baz4"])
mark_parametrize_version = pytest.mark.parametrize(
    "version", ["7.2.4-3", "0.git9.d5c3d43-1", "1:1.9.3-2"]
)
mark_parametrize_postfix = pytest.mark.parametrize("postfix", ["x86_64.pkg.tar.zst"])


class Test_Package:
    """Test the Package class."""

    @pytest.mark.parametrize(
        "prefix",
        [
            "file:///var/cache/pacman/pkg/",
            "https://ftp.agdsn.de/pub/mirrors/archlinux/core/os/x86_64/",
            "http://ftp.agdsn.de/pub/mirrors/archlinux/community/os/x86_64/",
        ],
    )
    @mark_parametrize_name
    @mark_parametrize_version
    @mark_parametrize_postfix
    def test_from_url(self, prefix, name, version, postfix):
        url = f"{prefix}{name}-{version}-{postfix}"
        package = Package.from_url(url)
        assert package.name == name
        assert package.version == version
        assert package.file is None
        assert package.removed_on is None

    @mark_parametrize_name
    @mark_parametrize_version
    @mark_parametrize_postfix
    def test_from_url_file(self, tmp_path, name, version, postfix):
        path = tmp_path / f"{name}-{version}-{postfix}"
        path.touch()
        url = f"file://{path}"
        package = Package.from_url(url)
        assert package.name == name
        assert package.version == version
        assert package.file == path
        assert package.removed_on is None

    @mark_parametrize_name
    @mark_parametrize_version
    @mark_parametrize_postfix
    def test_from_file(self, tmp_path, name, version, postfix):
        path = tmp_path / f"{name}-{version}-{postfix}"
        path.touch()
        package = Package.from_file(path)
        assert package.name == name
        assert package.version == version
        assert package.removed_on is None
        assert package.file == path

    @mark_parametrize_name
    @mark_parametrize_version
    def test_from_pacman_query(self, name, version):
        line = f"{name} {version}"
        package = Package.from_pacman_query(line)
        assert package.name == name
        assert package.version == version
        assert package.file is None
        assert package.removed_on is None

    @pytest.mark.parametrize(
        "a,b,result",
        [
            ("1.1-1", "1.2-1", True),
            ("1:1.1-1", "1.2-1", False),
            ("1.2-1", "1.2-1", False),
            ("1.2-1", "1.1-1", False),
        ],
    )
    def test_less_then(self, a, b, result):
        package_a = Package(name="foo", version=a)
        package_b = Package(name="foo", version=b)
        assert (package_a < package_b) is result

    def test_signature_file(self, tmp_path):
        package_path = tmp_path / "linux-5.17.2.arch3-1-x86_64.pkg.tar.zst"
        signature_path = package_path.with_suffix(package_path.suffix + ".sig")

        package_1 = Package.from_file(package_path)
        package_2 = Package(name=package_1.name, version=package_1.version)

        assert package_1.signature_file is None
        signature_path.touch()
        assert package_1.signature_file == signature_path
        assert package_2.signature_file is None

    def test_printing(self):
        package = Package(name="linux", version="5.17.2.arch3-1")
        assert str(package) == "linux 5.17.2.arch3-1"


class Test_pacman_config:
    def test_pacman_config(self, subprocess_run):
        with subprocess_run(stdout="test") as runner:
            result = _pacman_config("")
            assert runner.last_args == (("pacman-conf", ""),)
        assert result == "test"

    def test_pacman_db_dir(self, subprocess_run):
        with subprocess_run(stdout=str(Path.cwd())) as runner:
            result = pacman_db_dir()
            assert runner.last_args == (("pacman-conf", "DBPath"),)
        assert result == Path.cwd()

    def test_pacman_cache_dirs(self, subprocess_run, tmp_path):
        with subprocess_run(stdout=f"{tmp_path}\n{tmp_path}") as runner:
            result = pacman_cache_dirs()
            assert runner.last_args == (("pacman-conf", "CacheDir"),)
        assert result == [Path(tmp_path)] * 2

    def test_pacman_log_file(self, subprocess_run, tmp_path):
        log_file = tmp_path / "pacman.log"
        log_file.touch()
        with subprocess_run(stdout=str(log_file)) as runner:
            result = pacman_log_file()
            assert runner.last_args == (("pacman-conf", "LogFile"),)
        assert result == log_file


class Test_run_pacman:
    # TODO
    pass


class Test_pending_packages:
    def test_normal(self, subprocess_run, tmp_path):
        paths = [
            tmp_path / f"{name}-any.pkg.tar.zst"
            for name in ("zlib-1:1.2.11-5", "zstd-1.5.1-2")
        ]
        for path in paths:
            path.touch()
        stdout = "".join(f"file://{p}\n" for p in paths)
        with subprocess_run(stdout=stdout) as runner:
            pending = pending_packages(Path("tmp_db"), Path("log_file"))
            assert runner.last_args == (
                (
                    "pacman",
                    "--dbpath",
                    "tmp_db",
                    "--logfile",
                    "log_file",
                    "-Supq",
                ),
            )
        assert isinstance(pending, set)
        zlib, zstd = sorted(pending)
        assert zlib == Package(name="zlib", version="1:1.2.11-5")
        assert zlib.file == Path(f"{tmp_path}/zlib-1:1.2.11-5-any.pkg.tar.zst")
        assert zstd == Package(name="zstd", version="1.5.1-2")
        assert zstd.file == Path(f"{tmp_path}/zstd-1.5.1-2-any.pkg.tar.zst")

    def test_none(self, subprocess_run):
        with subprocess_run() as runner:
            pending = pending_packages(Path("tmp_db"), Path("log_file"))
            assert runner.last_args == (
                (
                    "pacman",
                    "--dbpath",
                    "tmp_db",
                    "--logfile",
                    "log_file",
                    "-Supq",
                ),
            )
        assert isinstance(pending, set)
        assert len(pending) == 0


class Test_installed_packages:
    def test_normal(self, subprocess_run):
        with subprocess_run(stdout="zlib 1:1.2.11-5\nzstd 1.5.1-2\n") as runner:
            installed = installed_packages()
            assert runner.last_args == (("pacman", "-Q"),)
        assert isinstance(installed, set)
        zlib, zstd = sorted(installed)
        assert zlib == Package(name="zlib", version="1:1.2.11-5")
        assert zstd == Package(name="zstd", version="1.5.1-2")

    def test_error(self, subprocess_run):
        with subprocess_run() as runner:
            with pytest.raises(
                RuntimeError, match="didn't identify any installed packages"
            ) as exec_info:
                installed_packages()
            assert runner.last_args == (("pacman", "-Q"),)


class Test_orphaned_packages:
    def test_normal(self, subprocess_run):
        with subprocess_run(stdout="zlib 1:1.2.11-5\nzstd 1.5.1-2\n") as runner:
            orphaned = orphaned_packages()
            assert runner.last_args == (("pacman", "-Qtd"),)
        assert isinstance(orphaned, set)
        zlib, zstd = sorted(orphaned)
        assert zlib == Package(name="zlib", version="1:1.2.11-5")
        assert zstd == Package(name="zstd", version="1.5.1-2")

    def test_none(self, subprocess_run):
        with subprocess_run() as runner:
            orphaned = orphaned_packages()
            assert runner.last_args == (("pacman", "-Qtd"),)
        assert isinstance(orphaned, set)
        assert len(orphaned) == 0


class Test_removed_packages:
    pass
