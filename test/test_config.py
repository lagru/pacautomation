"""Test config module."""

from dataclasses import fields, FrozenInstanceError
from pathlib import Path

import pytest

from pacautomation.config import Config


here = Path(__file__).parent

config_template_path = here / "../src/pacautomation.conf"


class Test_Config:
    default_options = dict(
        check_upgrades=True,
        download_upgrades=True,
        check_orphans=True,
        check_pacfiles=True,
        clean_cache=True,
        cache_expiry_days=14,
        log_file=Path("/var/log/pacautomation.log"),
    )

    def test_from_file(self):
        cfg = Config.from_file(config_template_path)
        # Check automatic type conversion based on annotations
        for field in fields(Config):
            value = getattr(cfg, field.name)
            assert isinstance(value, field.type)
        options = self.default_options.copy()

        for key, value in options.items():
            assert getattr(cfg, key) == value

    def test_frozen(self, tmp_path):
        cfg = Config(**self.default_options)
        for field in fields(cfg):
            with pytest.raises(FrozenInstanceError, match="cannot assign to field"):
                setattr(cfg, field.name, None)
