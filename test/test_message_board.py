"""Test `message_board` module."""

import os
import time
from pathlib import Path

from pacautomation import message_board


class Test_xdg_state_home:
    def test_not_set(self, monkeypatch):
        monkeypatch.delitem(os.environ, name="XDG_STATE_HOME", raising=False)
        result = message_board.xdg_state_home()
        assert result == Path.home() / ".local/state"

    def test_empty(self, monkeypatch):
        monkeypatch.setitem(os.environ, name="XDG_STATE_HOME", value="")
        result = message_board.xdg_state_home()
        assert result == Path.home() / ".local/state"

    def test_set(self, monkeypatch):
        monkeypatch.setitem(os.environ, name="XDG_STATE_HOME", value="/tmp/state")
        result = message_board.xdg_state_home()
        assert result == Path("/tmp/state")


class Test_NoticeBoard:
    def test_post_permission(self, tmp_path, monkeypatch, caplog):
        def patch(*args, **kwargs):
            raise PermissionError()

        board = message_board.MessageBoard(board_dir=tmp_path)
        with monkeypatch.context() as m:
            m.setattr("pathlib.Path.open", patch)
            board.post({"test": None})

        assert len(caplog.records) == 1
        assert "insufficient permissions" in caplog.records[0].message

    def test_clear_permission(self, tmp_path, monkeypatch, caplog):
        def patch(*args, **kwargs):
            raise PermissionError()

        board = message_board.MessageBoard(board_dir=tmp_path)
        dummy = board.board_dir / str(time.time_ns())
        dummy.touch()
        with monkeypatch.context() as m:
            m.setattr("pathlib.Path.unlink", patch)
            board.clear()

        assert len(caplog.records) == 1
        assert "insufficient permissions" in caplog.records[0].message


def test_posting_and_reading(tmp_path):
    board_dir = tmp_path / "spool"
    board_dir.mkdir()
    board = message_board.MessageBoard(board_dir=board_dir)
    reader_foo = message_board.MessageBoardReader(
        board_dir=board_dir, archive_dir=tmp_path / "foo"
    )
    note_0, note_1, note_2, note_3 = [{"number": i} for i in range(4)]

    board.clear()

    board.post(note_0)
    board.post(note_1)
    assert list(reader_foo) == [note_0, note_1]
    assert list(reader_foo) == []

    board.post(note_2)
    board.post(note_0)
    board.post(note_0)
    assert list(reader_foo) == [note_2, note_0, note_0]
    assert list(reader_foo) == []

    reader_bar = message_board.MessageBoardReader(
        board_dir=board_dir, archive_dir=tmp_path / "bar"
    )
    assert list(reader_bar) == [note_0, note_1, note_2, note_0, note_0]
    assert list(reader_bar) == []

    board.post(note_3)
    board.clear()
    assert list(reader_foo) == []

    board.post(note_0)
    assert list(reader_foo) == [note_0]
    board.clear()
    board.post(note_1)
    assert list(reader_foo) == [note_1]
