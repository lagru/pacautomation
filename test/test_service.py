"""Test service script."""

from pathlib import Path

import pytest

from pacautomation import service


@pytest.mark.parametrize(
    "file_no,st_size,expected",
    [
        (1, 2**30 - 1, "1024.00 MiB"),
        (2, 2**29, "1.00 GiB"),
    ],
)
def test_format_total_size(monkeypatch, file_no, st_size, expected):
    class MockStat:
        def __init__(self):
            self.st_size = st_size

    monkeypatch.setattr("pathlib.Path.stat", MockStat)
    files = [Path(f"/tmp/foo/{i}.tar.xz") for i in range(file_no)]
    result = service.format_total_size(files)
    assert result == expected


def test_reset_tmp_db(monkeypatch, tmp_path):
    def pacman_db_dir():
        return tmp_path / "bar"

    monkeypatch.setattr("pacautomation.service.pacman_db_dir", pacman_db_dir)
    pacman_db_dir().mkdir()
    tmp_db = tmp_path / "foo"

    with pytest.raises(ValueError, match="points to system's database"):
        service.reset_tmp_db(pacman_db_dir())
    assert not tmp_db.is_dir()

    service.reset_tmp_db(tmp_db)
    assert tmp_db.is_dir()

    # Reset again and test if a canary file is copied from pacman's directory
    (pacman_db_dir() / "canary.file").touch()
    service.reset_tmp_db(tmp_db)
    assert tmp_db.is_dir()
    assert (tmp_db / "canary.file").is_file()
