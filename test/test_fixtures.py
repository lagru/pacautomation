"""Test fixtures from conftest."""

import subprocess


def test_mock_runner(subprocess_run):
    with subprocess_run(0, stdout="this"):
        result = subprocess.run("echo this")
        assert result.stdout == "this"
        assert result.stderr is None
        assert result.returncode == 0
        assert result.args == ("echo this",)
