from setuptools import setup, find_packages

if __name__ == "__main__":
    setup(
        name="pacautomation",
        version="1.0.1",
        packages=find_packages("src"),
        package_dir={"": "src"},
        scripts=[
            "src/bin/pacautomation",
            "src/bin/pacautomation-notify",
        ],
    )
