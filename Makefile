# Automate common development and release tasks

src_files := $(shell git ls-files)
src_files += $(shell git ls-files --others --exclude-standard)
pkgname := pacautomation
pkgver := $(shell git describe --dirty='.dirty' | sed -E 's|v([0-9]*\.[0-9]*\.[0-9]*)-([0-9]*)-(.*)|\1+\2.\3|')
build_dir := build
makepkg_source_dest := $(build_dir)/$(pkgname)/src/$(pkgname)-v$(pkgver)
aur_dir := aur-repo


.PHONY: all
all: test local-package clean

.PHONY: update-version
update-version:
	sed -i 's|pkgver=".*"|pkgver="$(pkgver)"|' "PKGBUILD"
	sed -i 's|version=".*"|version="$(pkgver)"|' "setup.py"
	sed -i 's|__version__ = ".*"|__version__ = "$(pkgver)"|' "src/pacautomation/__init__.py"

# Build the local package archive locally from the current directory state
.PHONY: local-package
local-package: update-version
	rm -rf "$(makepkg_source_dest)"
	mkdir -p "$(makepkg_source_dest)"
	cp --parents -r $(src_files) "$(makepkg_source_dest)"
	PKGDEST="$(build_dir)" BUILDDIR="$(build_dir)" \
		makepkg  --force --cleanbuild --syncdeps --noextract
	namcap "$(build_dir)/$(pkgname)-$(pkgver)"*.tar.*

venv/bin/python:
	python -m venv venv
	venv/bin/python -m pip install --upgrade pip
	venv/bin/python -m pip install pytest-cov black ipython mypy
	venv/bin/python -m pip install -e .

.PHONY: lint
lint: venv/bin/python
	venv/bin/python -m black --check src/ test/ *.py
	venv/bin/python -m mypy --strict src

.PHONY: pytest
pytest: venv/bin/python
	venv/bin/python -m pytest --cov --doctest-modules

.PHONY: test
test: lint pytest

.PHONY: clean
clean:
	find build -depth -mindepth 1 -type d -exec rm -r "{}" \;
	rm -rf src/*.egg-info .pytest_cache htmlcov
	rm -f .coverage

#.PHONY: update-aur
#update-aur:
#	cp "PKGBUILD" "$(aur_dir)/"
#	cp "CHANGELOG.md" "$(aur_dir)/"
#	sed -i 's|source=()|source=("https://gitlab.com/lagru/pacautomation/-/archive/v$${pkgver}/$${pkgname}-v$${pkgver}.tar.gz")|' "$(aur_dir)/PKGBUILD"
#	updpkgsums "$(aur_dir)/PKGBUILD"
#	cd "$(aur_dir)" && makepkg --printsrcinfo > ".SRCINFO"
#	namcap "$(aur_dir)/PKGBUILD"
#
#.PHONY: build-aur
#build-aur: update-aur
#	cd "$(aur_dir)" && makepkg --force --cleanbuild --syncdeps
#	namcap "$(aur_dir)"/pacautomation-*-any.pkg.tar.*
