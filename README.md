# pacautomation

pacautomation is a small, lightweight background service that automates
periodic tasks around Arch Linux's [pacman](https://www.archlinux.org/pacman/).

With the default configuration the following tasks are executed weekly on
Mondays between 18:00 and 19:00 (local time):

- **Check for and download upgrades** to pacman's cache.
  If upgrades are available, inform all users with a desktop notification.
  A temporary package database is used to prevent [partial upgrades](https://wiki.archlinux.org/title/System_maintenance#Partial_upgrades_are_unsupported).
- **Clean the cache** off packages which were removed more than 14 days ago
  (default configuration).
  Packages that are still installed or are upgrades are always kept.
  If any files were deleted, inform all users with a desktop notification.
- **Check for [unused/orphaned packages](https://wiki.archlinux.org/title/Pacman/Tips_and_tricks#Removing_unused_packages_(orphans))**.
  If any orphans are found, warn all users with a desktop notification.
- **Check for unmerged [pacnew, pacsave and pacorig files](https://wiki.archlinux.org/title/Pacman/Pacnew_and_Pacsave)**.
  If any are found, warn all users with a desktop notification.

pacautomation is considered "lightweight" in the sense, that it does not run
continuously in the background but only when desired or required.
It tries to keep its dependencies minimal by relying on tooling which is
available on most minimal systems with a desktop environment.

pacautomation is currently used and tested on my system which uses KDE's
desktop environment Plasma.
The [notifications are sent with gdbus](https://wiki.archlinux.org/title/Desktop_notifications#Bash)
according to the [Desktop Notification Specification](https://specifications.freedesktop.org/notification-spec/notification-spec-latest.html)
so it *should work* on your system, e.g. Gnome, out of the box.

This started as a small convenience script for myself and grew pretty quickly
into a learning project, so it may be over-engineered somewhat. 😉
Feel free to try it out or [open an issue](https://gitlab.com/lagru/pacautomation/-/issues/new)
if you have feedback or questions!

**Contents:**

- [Installation](#installation)
- [Configuration](#configuration)
  - [Modifying the timer](#modifying-the-timer)
  - [Desktop notifications](#desktop-notifications)
- [Buugs!?](#buugs)
- [Similar solutions](#similar-solutions)
- [Copyright and licensing](#copyright-and-licensing)


## Installation

[pacautomation is available via the Arch user repository](https://aur.archlinux.org/packages/pacautomation/)
and can be installed with any [AUR helper](https://wiki.archlinux.org/title/AUR_helpers) 
or [manually](https://wiki.archlinux.org/title/Arch_User_Repository#Installing_and_upgrading_packages).

    git clone https://aur.archlinux.org/pacautomation.git
    cd pacautomation
    makepkg --install


## Configuration

pacautomation uses a configuration file at `/etc/pacautomation.conf`. A different
location can be set temporarily with the command line option `--config`.
[Have a look at the default configuration file](src/pacautomation.conf) to see
the available options and their documentation.


### Modifying the timer 

The periodic execution of pacautomation is controlled by a systemd timer which
can be modified with

    # systemctl edit pacautomation.timer

This will open an editor for an `override.conf` file in which you can 
[configure the timer](https://www.freedesktop.org/software/systemd/man/systemd.timer.html).
This file and configuration is persistent even if a new version of
pacautomation is installed.

The option `OnCalendar` controls when the timer triggers relative to the system
clock.
Each time the option is specified a new timer is added.
So if you want to replace the default configuration with your own calendar
event expression you need to first reset the option by assigning an empty
string.
E.g. 

    [Timer]
    OnCalendar=
    OnCalendar=Mon,Thu *-*-* 12:00:00

will ignore all preconfigured `OnCalendar` options and will execute the timer
twice a week on Monday and Thursday at 12:00 local time.
See the [time and date specifiation](https://www.freedesktop.org/software/systemd/man/systemd.time.html)
for more advanced calendar event expressions.

The periodic timer is enabled after the initial installation by default. To
disable the timer entirely run

    # systemctl disable --now pacautomation.timer

Upgrading pacautomation will not re-enable it.


### Desktop notifications

If pacautomation has something worthwhile to say it tries to send a desktop
notification to all active users.
On KDE Plasma, each user can configure this behavior in more detail under
*System Settings > Notifications > Applications*.


## Buugs?!

All pacautomation does should be logged to `/var/log/pacautomation.pacman.log`
(default configuration).
So if you get a notification like "Critical error while running pacautomation!"
or suspect an error you may find more details there or in [systemd's journal](https://wiki.archlinux.org/title/Systemd/Journal#Filtering_output).
You may also run pacautomation manually with the `--debug` option:

    # pacautomation --debug

Feel free to [open a bug report](https://gitlab.com/lagru/pacautomation/-/issues/new)
with the details you found.


## Similar solutions

If pacautomation is not for you, you may be interested in the following similar
solutions:

- [prep4ud](https://github.com/Cody-Learner/prep4ud): 
  A bash script for pre-downloading up-datable packages.
- [paccache](https://man.archlinux.org/man/paccache.8) and
  [pacdiff](https://man.archlinux.org/man/pacdiff.8):
  Maintenance scripts included in the [pacman-contrib package](https://archlinux.org/packages/community/x86_64/pacman-contrib/).
- [pkgcacheclean](https://aur.archlinux.org/packages/pkgcacheclean):
  Application to clean the pacman package cache.
- [pacleaner](https://aur.archlinux.org/packages/pacleaner):
  Script to clean up pacman cache in a more flexible way than with paccache or
  pacman itself. Fully configurable and flexible.

See also the wiki pages [Managing .pac* files](https://wiki.archlinux.org/title/Pacman/Pacnew_and_Pacsave#Managing_.pac*_files)
and [Cleaning the package cache](https://wiki.archlinux.org/title/Pacman#Cleaning_the_package_cache).


## Making a release

In this repository:

- Finalize release notes in `CHANGELOG.md`, update version numbers everywhere,
  and commit.
- Create a signed tag named `vX.Y.Z` including the version's release notes from
  the changelog.

On [pacautomation's AUR repo](https://aur.archlinux.org/packages/pacautomation):

- Update `PKGBUILD` and `CHANELOG.md` if necessary (compare with the ones in
  this repo).
- Run `updpkgsums` and update the package metadata with
  ```sh
  rm ".SRCINFO"
  makepkg --printsrcinfo > ".SRCINFO"
  ```
- Check if ` pkgrel=` is set correctly in `PKGBUILD`.
- Commit and push the above changes.


## Copyright and licensing

Copyright (C) 2021-2024, Lars Grüter

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
