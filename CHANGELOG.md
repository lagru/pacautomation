## Release 1.0.1 on 2024-10-18

- Fix too narrow restrictions of the service which broke downloading new
  packages in advance with pacman 7.0.0.

## Release 1.0.0 on 2024-03-15

- Update format and options of the configuration file. `pacautomation.conf` now
  uses the TOML format. New switches to control whether pacautomation checks
  for orphans or pacfiles were added. Checking for package updates and
  downloading them can now be independently configured. The option `tmp_db` to
  set the location of the temporary package database was removed.
- Warn if an unmerged `pacautomation.conf.pacnew` is found.
- Include more information in error messages. This applies to both, desktop
  notifications and the logged output.
- Use more fine-grained `ReadWritePaths` and restrict the main service to the
  files and directories it actually needs.
- Also, quite a bit refactoring was done on internals and the test suite was
  expanded.

## Release 0.18.1 on 2022-05-23

- Add missing desktop file to autostart the notifier.

## Release 0.18.0 on 2022-05-23

- pacautomation's functionality was split in two systems: `pacautomation` 
  handles the various automated service tasks in the background and is run with
  root privileges. It's triggered by a persistent systemd timer.
  `pacautomation-notify` checks for and displays pending desktop notifications.
  It is run for each user upon login or triggered after `pacautomation` 
  completes. This allows users to receive notifications even if they weren't
  logged in while `pacautomation` was run.
- Restrict the privileges and capabilities of the included systemd services.
  E.g. this prevents access to directories such as `/home` or `/root` for 
  `pacautomation.service`.
- Make pacautomation available as an Python package in the system's Python
  environment.
- Add a new desktop notification type for warnings.

## Release 0.17.0 on 2022-01-18

- Desktop notifications should now work on GNOME as well.
- Changed default name of the log file from `pacautomation.pacman.log` to
  `pacautomation.log`.
- The formatting of notification was tweaked slightly.
- All desktop notifications now use the urgency level "normal"; it was
  unspecified before.
- Improved sandboxing of pacautomation's background service. Many parts of the
  system are now read only or hidden such as `/home`.
- Extended the test suite.

## Release 0.16.3 on 2022-01-14

- Fix broken icon reference for informational notifications.

## Release 0.16.2 on 2022-01-14

- Use icons in notifications that are more consistent with KDE Plasma.
- Run very basic test suite when building the package.
- Include PYC files for all optimization levels in package.

## Release 0.16.1 on 2021-12-27

- Fix internal error which caused the script to fail if an orphaned package was
  found.
- Fix version comparison between packages; was only relevant for ordering the
  package order in the log and command line.

## Release 0.16.0 on 2021-12-22

- Add the command line option `--config` with which the default config file
  location can be overridden.
- Use less memory while handling packages (requires dataclass's `slots` keyword
  in Python 3.10).

## Release 0.15.1 on 2021-10-23

- Reported packages and files are now sorted with `vercmp`.
- Under the hood `pacman-conf` is used to get pacman's configuration.
- Improve performance when checking for expired packages and pacman's log file
  is large.
- Newly downloaded files are logged.

## Release 0.15.0 on 2021-10-12

- The pre-download of upgrades and cache cleaning is now optional. Both are
  enabled by default via two new options `enable` inside the configuration file.
- Introduced two new configuration sections `[UPGRADES]` and `[CACHE]`.
  Moved the configuration option `expiry_days` to a latter section.
- Upon first installation of the package, the user is reminded to enable the
  included systemd timer.
- Include this changelog in newly created pacautomation packages.
